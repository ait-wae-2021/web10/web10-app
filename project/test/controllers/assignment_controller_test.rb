require 'test_helper'

class AssignmentControllerTest < ActionDispatch::IntegrationTest
  test 'should get news' do
    get assignment_news_url
    assert_response :success
  end
end
