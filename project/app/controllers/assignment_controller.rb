require 'nokogiri'
require 'open-uri'
class AssignmentController < ApplicationController
  def news
    @resp = getBKKNews
    @resp2 = getWNews
  end

  def divide
    config.logger = ActiveSupport::Logger.new("log/#{Rails.env}.log")
    logger.info 'About to divide by 0'
    8 / 0
  rescue ZeroDivisionError => e
    @err = e.message
    @str = e.backtrace
    # raise
  end

  def getBKKNews
    article = Struct.new(:title, :desc, :url, :image)
    url = URI.parse('https://www.bangkokpost.com/world')
    doc = Nokogiri::HTML.parse(URI.open(url))
    headline = doc.xpath('//article').search('div.listnews-h//div.listnews-text//h3/a')
    url = doc.xpath('//article').search('div.listnews-h//div.listnews-text//h3//a/@href')
    description = doc.xpath('//article').search('div.listnews-h//div.listnews-text/p')
    image = doc.xpath('//article').search('div.listnews-h//div.listnews-img//a//img/@data-src')
    arr = []
    headline.each_with_index do |val, index|
      str = url[index].text
      str = 'https://www.bangkokpost.com' + str
      art = article.new(val.text, description[index].text, str, image[index].text)
      arr << art
    end
    arr
  end

  def getWNews
    article = Struct.new(:title, :desc, :url, :image)
    url = URI.parse('https://www.nytimes.com/section/world')
    doc = Nokogiri::HTML.parse(URI.open(url))
    headline = doc.xpath('//article').search('//h2/a')
    url = doc.xpath('//article').search('//h2/a/@href')
    # description = doc.xpath('//article').search('//p')
    image = doc.xpath('//article').search('//figure//a//img/@src')
    arr = []
    headline.each_with_index do |val, index|
      str = url[index].text
      str = 'https://www.nytimes.com' + str
      art = article.new(val.text, '', str, image[index])
      arr << art
    end
    arr
  end
end
