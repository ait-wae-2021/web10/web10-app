Rails.application.routes.draw do
  get 'assignment/news'
  get 'assignment/divide'
  get 'home/about'
  root 'home#index'
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth' }
end
